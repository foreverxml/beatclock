/* beatManager.js
 *
 * Copyright 2022 foreverxml <foreverxml@tuta.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


Plan:
  on boot: (in Promise)
    1. check time till next beat
    2. if <100ms (I think about potential lag here) handle code as if beat would have been passed already
    3. calculate current beat
    4. schedule loop at the correct ms time for next beat
    5. return beat in promise
  on loop: (in promise)
    1. get saved beat
    2. increase by one
    3. re-schedule loop at the correct ms time for next beat
    4. return beat in promise
*/
function setup() {
  return new Promise((resolve, reject) => {
    let currentMillis = new Date().getTime();
    let currentState = (currentMillis + 3600000) % 86400000;
    let distance = currentState % 86400;
    if (distance < 100) {
      currentState += 101
      currentMillis += 101
    }

  });
}
