/* window.js
 *
 * Copyright 2022 foreverxml <foreverxml@tuta.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';
import Adw from 'gi://Adw?version=1';

export const BeatclockWindow = GObject.registerClass({
    GTypeName: 'BeatclockWindow',
    Template: 'resource:///cc/amongtech/BeatClock/window.ui',
    InternalChildren: ['labelbeat', 'stack', 'beatbox', 'viewswitch', 'timebox', 'convert', 'sync'],
}, class BeatclockWindow extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });
        // where the functionality is!!!!
        let func = () => {
          this._labelbeat.set_text("@" + this._beatify());
          setTimeout(func, 4320);
        }
        this._viewswitch.connect('clicked', () => {
          if (this._stack.visible_child == this._sync.get_child()) {
          	this._stack.set_visible_child(this._convert.get_child());
          	this._viewswitch.set_label("Time");
          } else {
            this._stack.set_visible_child(this._sync.get_child());
            this._viewswitch.set_label("Convert");
          }
        });
        var timesbox, beatsbox, date;
        this._beatbox.connect('changed', () => {
          if (this._beatbox.get_buffer().get_text() == beatsbox) { return; }
          let time = this._debeatify(this._beatbox.get_buffer().get_text());
          let mins = time.getMinutes().toString();
          if (mins === "NaN") { this._timebox.get_buffer().set_text("", 0); return; }
          mins = mins.length == 1 ? "0" + mins : mins;
          let hous = time.getHours().toString();
          hous = hous.length == 1 ? "0" + hous : hous;
          timesbox = hous + ":" + mins;
          this._timebox.get_buffer().set_text(timesbox, 5);
        });
        this._timebox.connect('changed', () => {
          if (this._timebox.get_buffer().get_text() == timesbox) { return; }
          let time = this._timebox.get_buffer().get_text();
          if (time.length !== 5) { return; }
          date = new Date(2022, 2, 22, parseInt(time.substr(0,2)), parseInt(time.substr(3,4)), 0);
          beatsbox = this._beatify(date);
          this._beatbox.get_buffer().set_text(beatsbox, 3);
        });
        func();
    }
    _beatify(millis = new Date()) {
  	millis = (millis.getTime() + 3600000) % 86400000;
  	let currentBeat = Math.floor(millis / 86400);
  	let len = currentBeat.toString().length;
  	return len === 1 ? "00" + currentBeat
  		 : len === 2 ? "0" + currentBeat
  		 : currentBeat.toString();
    }
    _debeatify(beat = beatify()) {
  	 let date = new Date(parseInt(beat) * 86400);
  	 return date;
    }
});
